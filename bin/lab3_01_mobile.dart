import 'package:lab3_mobile/lab3_mobile.dart' as lab3_mobile;

import 'dart:io';

void main() {
  Menu s1 = Menu();
  s1.display();
}

class Compute {
  void display1() {
    print('Enter the value for x : ');
    int x = int.parse(stdin.readLineSync()!);
    print('Enter the value for y : ');
    int y = int.parse(stdin.readLineSync()!);
    print('Sum of the two numbers is : ');
    add(x, y);
  }
  void display2() {
    print('Enter the value for x : ');
    int x = int.parse(stdin.readLineSync()!);
    print('Enter the value for y : ');
    int y = int.parse(stdin.readLineSync()!);
    print('Sum of the two numbers is : ');
    sub(x, y);
  }
  void display3() {
    print('Enter the value for x : ');
    int x = int.parse(stdin.readLineSync()!);
    print('Enter the value for y : ');
    int y = int.parse(stdin.readLineSync()!);
    print('Sum of the two numbers is : ');
    mul(x, y);
  }
  void display4() {
    print('Enter the value for x : ');
    int x = int.parse(stdin.readLineSync()!);
    print('Enter the value for y : ');
    int y = int.parse(stdin.readLineSync()!);
    print('Sum of the two numbers is : ');
    divi(x, y);
  }
  
}

class Menu extends Compute {
  var choice = (stdin.readLineSync()!);

  void display() {
    print("MENU");
    print("Select the choice you want to perform");
    print("1. ADD");
    print("2. SUBTRACT");
    print("3. MULTIPLY");
    print("4. DIVIDE");
    print("5. EXIT");
    print("Choice you want to enter : ");
    var choice = (stdin.readLineSync()!);
    switch (choice) {
      case "1":
        {
          super.display1();
        }
        break;
      case "2":
        {
          super.display2();
        }
        break;
      case "3":
        {
          super.display3();
        }
        break;
      case "4":
        {
          super.display4();
        }
        break;
      case "5":
        {
          print("Exit Program Calcultor.");
        }
        break;
    }
  }
}

void add(int x, int y) {
  int sum = x + y;

  print('$sum');
}

void sub(int x, int y) {
  int sum = x - y;

  print('$sum');
}

void mul(int x, int y) {
  int sum = x * y;

  print('$sum');
}

void divi(int x, int y) {
  double sum = x / y;

  print('$sum');
}
